# How to Create A Quick Action in Mac OS

The quick action can be used to create shortcut.
I found this tip while I was looking for a method that can launch the terminal app like iTerm with a short cut.

## Method

1. First, open `Automator` app and create new `Quick Action` document.
2. Configure it so it receives `no input` in `any application`
3. Add a `Run AppleScript` action and set its contents to the following:
```
on run {input, parameters}
     tell application "iTerm"
         create window with default profile
     end tell
     return input
end run
```
4. Save the document and create a keyboard shortcut at the `System Preferences`.
	- Go to `Shortcuts` tab in the `Keyboard`.
	- You can find your saved quick action under the `General` section.
