
# Useful Collection of Git Command (And Hopefully be a Guide)

## Basic Usage

Add your changes in the git stage

```bash
# For specific files
git add file_1 file_2
# For entire files
git add .
```

You can commit your change

```bash
git commit -m "Message"
```

Now, ready to push your changes to your branch,

```bash
git push
```

If you want to change your working branch,

```bash
git checkout feature/branch_name
```

And your branch can be merged by using

```bash
git merge feature/branch_name target_branch
```

In this case, if you use `--no-ff`, it will preserve all commits in your `feature/branch_name` and keep away from `target_branch`. This will mess up to commit structure if the project size getting bigger and bigger. However, this can help to identify your changes in the future.

## Git Log

You can view commit history using `git log` command.
Some useful options are shown below.

### Diffs
The `--stat` option displays the number of insertions and deletions of each commit.

``` bash
git log --stat
```

Which gives

```bash
commit 61af0b73ffdc0d636670bfc6a9ae8a04a04b1a25
Author: Kyoungseoun Chung <kchung@student.ethz.ch>
Date:   Fri Dec 31 18:51:23 2021 +0100

    MES: now, we are using torch.tensor for all variables. However, with different device either cpu or cuda

 pyABC/boundaries.py  | 43 +++++++++++++++----------------------------
 pyABC/diagnostics.py | 10 ++++++----
 pyABC/fields.py      | 14 +++++++-------
 pyproject.toml       |  4 ++++
 tests/test_bcs.py    | 18 +++++++++---------
 5 files changed, 41 insertions(+), 48 deletions(-)
```

### Graphs
The `--graph` option draws an ASCII graph of your branch structure. For better visibility, you can combine `--oneline` and `--decorate` option too.

```bash
git log --graph --oneline --decorate
```

Which gives

```bash
# Output
# Copied below from  https://www.atlassian.com/git/tutorials/git-log
*   0e25143 (HEAD, main) Merge branch 'feature'
|\
| * 16b36c6 Fix a bug in the new feature
| * 23ad9ad Start a new feature
* | ad8621a Fix a critical security issue
|/
* 400e4b7 Fix typos in the documentation
* 160e224 Add the initial code base
```



## Git Stash

## Git Push without CI

Sometimes, you come up with some ideas and try to implement a feature. However, the feature is not yet completed, therefore, it is expected to be failed during the CI/CD process.

One thing you can do is, avoid `git push` and continue coding until the feature is settle down, but this is extremely difficult since you have a forced habit of typing `git commit -m "message"` and `git push` regularly.

In this case, you can make the commit ignore the CI process (adding `[ci skip]` in commit message) typing followings:

```bash
git commit -m "Message ... [ci skip]" 	# This let git push ignore the CI process
git push
```
