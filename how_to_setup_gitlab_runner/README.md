# How to Setup Gitlab Runner Using Docker

You can find the detailed information about how to setup the Runner at [GitLab website](https://docs.gitlab.com/runner/install/)

However, the amount of information is too huge so that it is a bit difficult to grasp right way to set up the Runner.
Therefore, I picked processes that I only need, and summarize.

**Note**: I assume that you already installed the [Docker](https://www.docker.com/) in your system (Linux).

## Install the Docker image and start the container

1. Use local system volume mounts to start the Runner container

```bash
docker run -d --name gitlab-runner --restart always \
     -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest
```
- Here, configuration file `config.toml` will be stored in `/etc/gitlab-runner/`

## Register the Runner

1. Run the register command:

```bash
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner \
		gitlab/gitlab-runner:latest register
```

- To complete the process, you need GitLab instance URL and token. Those can be found from your repository `setting -> CI/DI -> Runners` section.

2. After that, you will receive message regarding the instance URL, token, description for the runner, tags associated with the runner, the runner executor (probably `docker` for our case), and the default image to be used for the project.

## Reading GitLab Runner logs

1. You can check the logs with:

```bash
docker logs gitlab-runner
```

## Some Docker Commands

1. Check container's [status](https://docs.docker.com/engine/reference/commandline/ps/)

```bash
docker ps [OPTIONS]
```
- `-a` flag will apply to all images

2. Remove [unused data](https://docs.docker.com/engine/reference/commandline/system_prune/)

```bash
docker system prune [OPTIONS]
```
- `-a` flag will apply to all images
