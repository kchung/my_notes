# Collection of Useful Information

## Table of Contents

- [How to setup the GitLab Runner](./how_to_setup_gitlab_runner/README.md)
- [How to use different Python version using Pyenv](./how_to_use_pyenv/README.md)

