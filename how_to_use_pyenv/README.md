# How to Use PYENV for Switching Different Version of Python(s)

Sometimes (especially you are staying in the realm of the Arch Linux), you may encounter the issue of latest version of the Python. (This is mainly due to the fact that the Arch Linux try to keep the latest version of the packages.) Also, in many cases, a new version of the Python causes breakdown of the dependencies in the software libraries.

In this regards, managing different version of Pythons are (usually) required and [pyenv](https://github.com/pyenv/pyenv) is the simplest tool to accomplish what we need.

## Installation

If you are using the Arch Linux, you can simply install the package from [AUR](https://archlinux.org/packages/community/any/pyenv/).

For example, using [paru](https://github.com/Morganamilo/paru)

```zsh
paru pyenv
```

Otherwise, you need to install by git:

```zsh
 git clone https://github.com/pyenv/pyenv.git ~/.pyenv
 # Below is optional but will speed up the pyenv
 cd ~/.pyenv && src/configure && make -C src
```

## Configure shell's environment for Pyenv

For zsh shell, copy and paste bellows in to your terminal

```zsh
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zprofile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zprofile
echo 'eval "$(pyenv init --path)"' >> ~/.zprofile

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
echo 'eval "$(pyenv init --path)"' >> ~/.profile

echo 'eval "$(pyenv init -)"' >> ~/.zshrc
```

If you are using different shell, check Pyenv [website](https://github.com/pyenv/pyenv)

Note: After the configure, you need to log out and in the system to apply the changes.

## How to use

First install desired python version using the command,

```zsh
pyenv install --list 		# check the list of all supported python versions
pyenv install 3.9.9
```

and switch the version using `global` option:

```zsh
pyenv global 3.9.9
python -V 		# This should give Python 3.9.9 as the result

```

Now, you can work with different version of the Python in your system.

## Uninstall

To uninstall, simply remove Pyenv root directory:

```bash
rm -rf $(pyenv root)
```
